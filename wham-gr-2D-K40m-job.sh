#PBS -N WHAM_GROSSFIELD_BUTANE_2D
#PBS -q GPUQ
#PBS -l nodes=1:ppn=1:seriesGPUk


# Please leave the hostname command here for troubleshooting purposes.
hostname


# Your science stuff goes here:
#/home/apotgieter/GROSSFIELD_BUTANE_2D/H19x19/B30x30/run_gr_2D.sh

/home/apotgieter/GROSSFIELD_BUTANE_2D/H19x19/run_all.sh
/home/apotgieter/GROSSFIELD_BUTANE_2D/H37x37/run_all.sh
/home/apotgieter/GROSSFIELD_BUTANE_2D/H61x61/run_all.sh
/home/apotgieter/GROSSFIELD_BUTANE_2D/H91x91/run_all.sh
